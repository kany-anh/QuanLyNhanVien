var DSNV = [];

let dataJson = localStorage.getItem("DSNV_local");

if (dataJson != null) {
  var dataArr = JSON.parse(dataJson);
  DSNV = dataArr.map(function (item) {
    var nhanVien = new Nhanvien(
      item.tknv,
      item.ten,
      item.email,
      item.pass,
      item.ngayLam,
      item.luongCB,
      item.chucVu,
      item.gioLam
    );
    return nhanVien;
  });
  renderDSNV(DSNV);
}

function themNV() {
  var nhanVien = layThongTinTuFrom();
  var isValid = true;
  isValid =
    kiemTraKhongDuocDeTrong(nhanVien.tknv, "tbTKNV") &&
    kiemTraSo(nhanVien.tknv) &&
    kiemTraTrung(nhanVien.tknv, DSNV) &&
    kiemTraDoDai(nhanVien.tknv, "tbTKNV", 4, 6);
  isValid =
    isValid &
    (kiemTraKhongDuocDeTrong(nhanVien.email, "tbEmail") &&
      kiemTraEmail(nhanVien.email));
  isValid =
    isValid &
    (kiemTraKhongDuocDeTrong(nhanVien.ten, "tbTen") &&
      kiemTraChu(nhanVien.ten));
  isValid =
    isValid &
    (kiemTraKhongDuocDeTrong(nhanVien.pass, "tbMatKhau") &&
      kiemTraMatKhau(nhanVien.pass));
  isValid = isValid & kiemTraKhongDuocDeTrong(nhanVien.ngayLam, "tbNgay");
  isValid =
    isValid &
    (kiemTraKhongDuocDeTrong(nhanVien.luongCB, "tbLuongCB") &&
      kiemTraLuong(nhanVien.luongCB));
  isValid = isValid & kiemTraKhongDuocDeTrong(nhanVien.chucVu, "tbChucVu");
  isValid =
    isValid &
    (kiemTraKhongDuocDeTrong(nhanVien.gioLam, "tbGiolam") &&
      kiemTraGioLam(nhanVien.gioLam));

  console.log(isValid);
  if (isValid) {
    DSNV.push(nhanVien);
    var dsnvJson = JSON.stringify(DSNV);
    localStorage.setItem("DSNV_local", dsnvJson);
    renderDSNV(DSNV);
  }
}

function capNhatNV() {
  var nhanVien = layThongTinTuFrom();
  var taiKhoan = document.getElementById("tknv").value * 1;
  var ViTri = timKiemViTri(taiKhoan, DSNV);
  if (ViTri != -1) {
    DSNV.splice(ViTri, 1, nhanVien);
    var dsnvJson = JSON.stringify(DSNV);
    localStorage.setItem("DSNV_local", dsnvJson);
    renderDSNV(DSNV);
    console.log(nhanVien);
  }
}

function xoaNV(idNV) {
  var ViTri = timKiemViTri(idNV, DSNV);
  if (ViTri != -1) {
    DSNV.splice(ViTri, 1);
    var dsnvJson = JSON.stringify(DSNV);
    localStorage.setItem("DSNV_local", dsnvJson);
    renderDSNV(DSNV);
  }
}

function suaNV(idNV) {
  var ViTri = timKiemViTri(idNV, DSNV);
  if (ViTri != -1) {
    var nhanVien = DSNV[ViTri];
    document.getElementById("tknv").value = nhanVien.tknv;
    document.getElementById("name").value = nhanVien.ten;
    document.getElementById("email").value = nhanVien.email;
    document.getElementById("password").value = nhanVien.pass;
    document.getElementById("datepicker").value = nhanVien.ngayLam;
    document.getElementById("luongCB").value = nhanVien.luongCB;
    document.getElementById("chucvu").value = nhanVien.chucVu;
    document.getElementById("gioLam").value = nhanVien.gioLam;

    document.getElementById("tknv").disabled = true;
  }
}
